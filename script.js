
//get data

const getData = () => {
    fetch("https://jsonplaceholder.typicode.com/users").then(response => response.json())
    .then(data => {
        let users = "";
        return data.forEach(user => {
            users += `
                <div>
                    <h3>${user.name}</h3>
                    <p>${user.email}</p>
                    <p>${user.company}</p>
                    <p>${user.phone}</p>
                </div>
            `
            document.querySelector("#output").innerHTML = users;
        })
      
    })
    .catch(err => {
        console.log("rejected", err)
    })
}

//Add Post
//e is stands for event object
const addPost = (e) => {
    e.preventDefault();
    let name = document.querySelector("#name").value;
    let email = document.querySelector("#email").value;
    let company = document.querySelector("#company").value;
    let phone = document.querySelector("#phone").value;

    fetch("https://jsonplaceholder.typicode.com/users", {
        method: "POST",
        headers: {
            "Accept": "application/json, text/plain",
            "Content-type": "application/json"
        },
        body: JSON.stringify({name: name, email: email, company: company, phone: phone})
    }).then(response => response.json())
    .then(data => console.log(data))
    .catch(err => {
        console.log(err)
    })

}

let btn1 = document.querySelector("#getData");
btn1.addEventListener("click", getData);

const addForm = document.querySelector("#addUser");
addForm.addEventListener("submit", addPost);